!
!  Robust mean
!
!  Copyright © 2024-2025 F.Hroch (hroch@physics.muni.cz)
!
!  This file is part of Oakleaf.
!
!  Oakleaf is free software: you can redistribute it and/or modify
!  it under the terms of the GNU Lesser General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  Oakleaf is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU Lesser General Public License for more details.
!
!  You should have received a copy of the GNU Lesser General Public License
!  along with Oakleaf.  If not, see <http://www.gnu.org/licenses/>.
!
!

module robustmean_REAL64

  ! This module provides rmean subroutine for estimation of robust mean.
  !
  ! rmean should be called as:
  !
  !  call rmean(data,mean,stderr,stdsig,scale,flag,verbose,report)
  !
  ! on input:
  !   data - array of data values to be estimated
  !   verbose (optional) - print additional informations
  !
  ! on output are estimated:
  !   mean - robust mean
  !   stderr (optional) - standard error
  !   stdsig (optional) - standard deviation
  !   scale (optional) - scale of function normalisation
  !   flag (optional) - indicates reliability of results:
  !          0 == success, results are both reliable and accurate
  !          1 == consider a rough estimate due convergence fail, try verbose
  !          2 == basic estimates, few datapoints available
  !          3 == data looks nearly identical
  !          8 == failed to allocate memory
  !
  ! The results gives the robust estimate of the true value X
  ! of the sample x, with 68% probability, into the interval
  !
  !         mean - stderr  <  X  <  mean + stderr
  !
  ! Data distribution is estimates as Normal distribution with the parameters
  !
  !          N(mean,stdsig).
  !
  ! Robust estimators has been prepared on base of
  !  * Hogg in Launer, Wilkinson: Robustness in Statistics
  !  * Huber: Robust Statistics
  !  * my experiences

  use iso_fortran_env

  implicit none
  private

  public :: rmean_REAL64

contains

  subroutine rmean_REAL64(data,mean,stderr,stdsig,scale,reltol,robfun,rankinit, &
       flag,verbose,report)

    ! This routine finds robust mean by looking for the maximum of likelihood.

    use sqpfmean_module_REAL64
    use rlike_REAL64
    use quicksort
    use quickmedian

    real(REAL64), dimension(:), target, intent(in) :: data
    real(REAL64), intent(in out) :: mean
    real(REAL64), intent(out), optional :: stderr, stdsig
    real(REAL64), intent(in out), optional :: scale
    real(REAL64), intent(in), optional :: reltol
    character(len=*), intent(in), optional :: robfun
    logical, intent(in), optional :: rankinit
    integer, intent(out), optional :: flag
    logical, intent(in), optional :: verbose
    character(len=*), intent(in), optional :: report

    type(robust_likelihood_hampel), target :: hampelfun
    type(robust_likelihood_tukey), target :: tukeyfun
    type(robust_likelihood_huber), target :: huberfun
    type(nonrobust_likelihood_square), target :: squarefun
    class(robust_likelihood_REAL64), pointer :: fcn

    integer, parameter :: N1E6 = 2**20
    real(REAL64), parameter :: q34 = 0.6745
    real(REAL64), dimension(:), allocatable :: xcdf
    real(REAL64), dimension(2,2) :: B
    real(REAL64) :: t,dt,s,s0,t0,sig,rtol
    integer :: N, NK, NN, K, m, i, aflag, info, mmax
    logical :: verb, rinit, siter, converge
    character(len=666) :: modifier, errmsg !oaklef_robfun
    character(len=:), allocatable :: report_file

    verb = .false.
    rinit = .true.
    rtol = 1.0 / 42.0
    N = size(data)
    report_file = ''
    if( present(verbose) ) verb = verbose
    if( present(flag) ) flag = 6
    if( present(reltol) ) rtol = reltol
    if( present(report) ) report_file = report

!!$    call get_environment_variable("OAKLEAF_ROBFUN",oaklef_robfun)
!!$    if( status == 0 ) then
!!$       read(oaklef_robfun,*) robfun
!!$    end if

    if( present(rankinit) ) then
       rinit = rankinit
       if( .not. rinit ) then
          if( .not. present(scale) ) then
             if( verb ) write(error_unit,*) &
                  "rmean(): specify mean & scale, if rankinit == .false."
             if( present(flag) ) flag = 5
             return
          end if
       end if
    end if

    if( present(robfun) ) then
       modifier = 'robust'
       if( robfun == 'huber' ) then
          fcn => huberfun
       else if( robfun == 'tukey' ) then
          fcn => tukeyfun
       else if( robfun == 'square' ) then
          fcn => squarefun
          modifier = 'NON-robust'
       else
          fcn => hampelfun
       end if
       if( verb ) write(error_unit,*) &
            'The selected ',trim(modifier),' function: ',trim(robfun)
    else
       fcn => hampelfun
!       fcn => tukeyfun
       if( verb ) write(error_unit,*) &
            'The robust function, by default: ',"Hampel's"
    end if
    fcn%data => data


    if( n == 0 ) then

       ! results are undefined
       if( present(flag) ) flag = 5
       if( verb ) write(error_unit,* ) &
            'rmean: finish for no points (all parameters are undefined)'
       return

    else if( n == 1 ) then

       ! dispersion and std.err. are undefined
       mean = data(1)
       if( present(stderr) )   stderr = 0 ! Nan
       if( present(stdsig) )   stdsig = 0 ! Nan
       if( present(scale) )    scale = 1  ! Nan
       if( present(flag) )     flag = 2
       if( verb ) write(error_unit,*) 'rmean: ',mean,' stderr:',0,' stdsig:',0, &
            ' scale:',1,' (finish for a single point)'
       return

    else if( n == 2 ) then

       mean = sum(data) / N
       s = norm2(data-mean)
       sig = s
       if( present(stderr) )   stderr = sig / sqrt(real(N,REAL64))
       if( present(stdsig) )   stdsig = sig
       if( present(scale) )    scale = s ! incorrect
       if( present(flag) )     flag = 2
       if( verb ) write(error_unit,*) 'rmean: ',mean,' stderr:',s/sqrt(2.0), &
            ' stdsig:',s,' scale:',s,' (finish for a pair)'
       return

    end if

    !
    ! ... n > 2, below this point ...
    !
    ! values are initialised by quantiles (by median)
    m = 0
    mmax = 1
    siter = .false.
    if( rinit ) then

       siter = N <= 42   ! should be N < N1E6
       K = max(N / N1E6, 1)
       NN = N / K
       NK = K*NN
       allocate(xcdf(NN),stat=aflag,errmsg=errmsg)
       if( aflag /= 0 ) then
          if( verb ) write(error_unit,*) 'rmean() allocate: ',trim(errmsg)
          if( present(flag) ) flag = 8
          return
       end if
       if( siter ) then
          xcdf = data
          call qsort(xcdf)
          t0 = (xcdf(N/2+1) + xcdf((N+1)/2)) / 2
          xcdf = abs(data - t0)
          call qsort(xcdf)
          m = N / 2 + 1
          s0 = xcdf(m) / q34
          mmax = N - m  ! N > m, it requires N > 2
          ! deallocate() is postponed.
       else
          xcdf = data(1:NK:K)
          t0 = qmed(xcdf,NN/2)
          xcdf = abs(data(1:NK:K) - t0)
          s0 = qmed(xcdf,NN/2) / q34
          deallocate(xcdf)
       end if
       if( verb ) write(error_unit,'(a,g0,a,es12.5,3a)') &
            "rob.init: t,s,dt=",t0," (median),",s0," (MAD)"
    else
       t0 = mean
       s0 = scale
       if( verb ) write(error_unit,*) "rob.init by user: t,s=",t0,s0
    end if

    if( abs(s0) < 42*epsilon(s0) ) then
       ! If more than half of data has (nearly) identical values,
       ! the scale estimate is (close to) zero. No application of
       ! robust methods is possible violating s0 > 0.
       !
       ! All the data are supposed to be identical (a few points
       ! can still deviate), result can be considered as reliable.

       mean = t0
       if( present(stderr) ) stderr = 0
       if( present(stdsig) ) stdsig = 0
       if( present(scale) )  scale = s0
       if( present(flag) )   flag = 3

       if( verb ) write(error_unit,*) 'rmean: ',t0,' stderr:',0,' stdsig:',0, &
            ' scale:',s0,' (finish for identical points)'
       return
    end if

    do i = 1, mmax

       t = t0
       s = s0
       call sqpfmean_REAL64(fcn,t,s,B,rtol,verb,report_file,info)

       if( .not. (info == 10 .and. siter) ) exit
       m = m + 1
       s0 = xcdf(m) / q34
       if( verb ) write(error_unit,'(a,es12.5)') 'Scale by MAD increased on ',s0
    end do
    converge = info == 0 .or. info == 1

    if( converge ) then
       dt = 1/sqrt((N-1)*B(1,1))
       sig = sqrt((N/(N-1.0))/B(1,1))
    else
       t = t0
       s = s0
       dt = s0 / sqrt(real(N,REAL64))
       sig = s
    end if

    if( verb .and. converge ) then
       block
         use rfun

         real(REAL64), dimension(2,2) :: hess
         character(len=80) :: fmt
         integer :: nrob,nplus,i
         integer, dimension(3) :: nsig
         real(REAL64), dimension(:), allocatable :: res

         fmt = '(a,g0,3(a,es9.2),1x,2l1)'
         write(error_unit,fmt) &
              'rmean: ',t,' stderr:',dt,' stdsig:',sig,' scale:',s,converge
         block
           real(REAL64) :: t,stdsig,stderr
           t = sum(fcn%data)/n
           stdsig = sqrt(sum((fcn%data - t)**2)/(n-1.0))
           stderr = stdsig / sqrt(1.0*n)
           write(error_unit,fmt) 'amean: ',t,' stderr:',stderr,' stdsig:',stdsig
         end block

         write(error_unit,'(a,20x,2(a,es9.2))') ' by H:', &
              ' stderr:',fcn%stderr(t,s), &
              ' stdsig:',fcn%stderr(t,s)*sqrt(1.0*N)

         call fcn%hess(t,s,hess)
         fmt = '(a,4es13.5,3x,a,es8.2e2)'
         write(error_unit,fmt) 'B:',B
         write(error_unit,fmt) 'H:',hess,"|B-H|/|H|: ",norm2(B-hess)/norm2(hess)

         ! sign test
         allocate(res,source=fcn%data)
         res = (fcn%data-t)/sig
         nplus = count(res > 0)
         write(error_unit,'(a,i0,"/",f0.1,", ",i0,"+-",f0.1)') &
              'Sign test (total/expected, +): ', &
              n,n/2.0,nplus, sqrt(nplus*0.25 + 1e-6)
         if( robfun == 'huber' ) then
            res = huber((data - t)/s)
         else if( robfun == 'tukey' ) then
            res = tukey((data - t)/s)
         else if( robfun == 'square' ) then
            res = (data - t)/s
         else
            res = hampel((data - t)/s)
         end if
         nrob = count(abs(res) > epsilon(res))
         nplus = count(res > 0 .and. abs(res) > epsilon(res))
         write(error_unit,'(a,i0,"/",f0.1,", ",i0,"+-",f0.1)') &
              'Sign test with rob.function (total/expected, +): ', &
              nrob,nrob/2.0,nplus,sqrt(nplus*0.25 + 1e-6)
         res = (fcn%data-t)/sig
         do i = 1,3
            nsig(i) = count(abs(res) < i)
         end do
         write(error_unit,'(a,3(i0,"-sig: ",f0.1,"%":",  "))') &
              'Points within the interval:  ', (i,100.0*(nsig(i)/real(n)),i=1,3)
         write(error_unit,'(a,f0.1,"%")') &
              'Points out of the interval 5-sig: ',100*(count(abs(res) > 5)/real(n))

       end block
    end if

    mean = t
    if( present(stderr) ) stderr = dt
    if( present(stdsig) ) stdsig = sig
    if( present(scale) ) scale = s
    if( present(flag) ) flag = info

  end subroutine rmean_REAL64

end module robustmean_REAL64

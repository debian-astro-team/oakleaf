!
!  Hampel robust minimization function
!
!  Copyright © 2022-2025 F.Hroch (hroch@physics.muni.cz)
!
!  This file is part of Oakleaf.
!
!  Oakleaf is free software: you can redistribute it and/or modify
!  it under the terms of the GNU Lesser General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  Oakleaf is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU Lesser General Public License for more details.
!
!  You should have received a copy of the GNU Lesser General Public License
!  along with Oakleaf.  If not, see <http://www.gnu.org/licenses/>.
!

module rfun_hampel_REAL32

  use iso_fortran_env
  implicit none
  private

  real(REAL32), parameter :: a = 1.7_REAL32
  real(REAL32), parameter :: b = 3.4_REAL32
  real(REAL32), parameter :: c = 8.5_REAL32

  real(REAL32), parameter :: R = 0.5_REAL32
  real(REAL32), parameter :: q = a/(c - b)
  real(REAL32), parameter :: half = 0.5_REAL32
  real(REAL32), parameter :: halfq = q / 2
  real(REAL32), parameter :: rr = 1 / (2*R)
  real(REAL32), parameter :: za = 2*a - R/2
  real(REAL32), parameter :: zb = -2*a/q + R/2
  real(REAL32), parameter :: zc = -R/2
  real(REAL32), parameter :: a1 = a - R, a2 = a + R
  real(REAL32), parameter :: b1 = b - R, b2 = b + R
  real(REAL32), parameter :: c1 = c - R, c2 = c + R
  real(REAL32), parameter :: da = (a-R)**2 + za*R - 2*R**2/3
  real(REAL32), parameter :: d1 = (za*R + R**2/3 + da)/2 - a*(a + R)
  real(REAL32), parameter :: db = zb*R - R**2/3 - 2*(a*(b-R) + d1)/q
  real(REAL32), parameter :: &
         d2 = -(db + zb*R + 2*R**2/3)/2 + (b+R)**2/2 - c*(b+R)
  real(REAL32), parameter :: &
       dc = zc*R - 2*R**2/3 - 2*(c*(c-R) - (c-R)**2/2 + d2)
  real(REAL32), parameter :: d3 = -q*(zc*R + R**2/3 + dc)/2 ! =8.6283
  real(REAL32), parameter :: d = a*b - a**2/2 - q*(b*c - b**2/2)
  real(REAL32), parameter :: six = q*c**2/2 + d ! =8.6700

  public :: ipshampel_REAL32, ihampel_REAL32, pshampel_REAL32, hampel_REAL32, &
       dpshampel_REAL32, dhampel_REAL32

contains

  elemental pure function pshampel_REAL32(x) result(pshampel)

    real(REAL32):: pshampel
    real(REAL32), intent(in) :: x
    real(REAL32) :: u

    u = abs(x)
    if( u < a1 ) then
       pshampel = x
    else if( u < a2 ) then
       u = u - a
       pshampel = half*sign(za + u*(1 - rr*u),x)
    else if( u < b1 ) then
       pshampel = sign(a,x)
    else if( u < b2 ) then
       u = u - b
       pshampel = halfq*sign(zb + u*(1 + rr*u),x)
    else if( u < c1 ) then
       pshampel = sign(q*(c - u),x)
    else if( u < c2 ) then
       u = u - c
       pshampel = halfq*sign(zc + u*(1 - rr*u),x)
    else
       pshampel = 0
    endif

  end function pshampel_REAL32

  elemental pure function dpshampel_REAL32(x) result(dpshampel)

    real(REAL32) :: dpshampel
    real(REAL32), intent(in) :: x
    real(REAL32), parameter :: R1 = 1 / R
    real(REAL32) :: u

    u = abs(x)
    if( u < a1 ) then
       dpshampel = 1
    else if( u < a2 ) then
       dpshampel = half*(1 - (u - a)*R1)
    else if( u < b1 ) then
       dpshampel = 0
    else if( u < b2 ) then
       dpshampel = -halfq*(1 + (u - b)*R1)
    else if( u < c1 ) then
       dpshampel = -q
    else if( u < c2 ) then
       dpshampel = -halfq*(1 - (u - c)*R1)
    else
       dpshampel = 0
    endif

  end function dpshampel_REAL32


  elemental pure function ipshampel_REAL32(x) result(ipshampel)

    real(REAL32):: ipshampel
    real(REAL32), intent(in) :: x
    real(REAL32), parameter :: r6 = 1 / (6*R)
    real(REAL32) :: u

    u = abs(x)
    if( u < a1 )then
       ipshampel = half*x**2
    else if( u < a2 ) then
       u = u - a
       ipshampel = half*(da + u*(za + u*(half - r6*u)))
    else if( u < b1 ) then
       ipshampel = a*u + d1
    else if( u < b2 ) then
       u = u - b
       ipshampel = -halfq*(db + u*(zb + u*(half + r6*u)))
    else if( u < c1 ) then
       ipshampel = q*(u*(c - half*u) + d2)
    else if( u < c2 ) then
       u = u - c
       ipshampel = -halfq*(dc + u*(zc + u*(half - r6*u)))
    else ! if ( u >= c ) then
       ipshampel = d3
    end if

  end function ipshampel_REAL32

  elemental pure function hampel_REAL32(x) result(hampel)

    real(REAL32):: hampel
    real(REAL32), intent(in) :: x
    real(REAL32) :: u

    u = abs(x)
    if( u < a )then
       hampel = x
    else if( a <= u .and. u < b )then
       hampel = sign(a,x)
    else if( b <= u .and. u < c )then
       hampel = sign(q*(c - u),x)
    else ! if( u >= c )then
       hampel = 0
    endif

  end function hampel_REAL32

 elemental pure function dhampel_REAL32(x) result(dhampel)

    real(REAL32) :: dhampel
    real(REAL32),intent(in) :: x
    real(REAL32) :: u

    u = abs(x)
    if( u < a )then
       dhampel = 1
    else if( b <= u .and. u < c )then
       dhampel = -q
    else
       dhampel = 0
    endif

  end function dhampel_REAL32

  elemental pure function ihampel_REAL32(x) result(ihampel)

    real(REAL32):: ihampel
    real(REAL32), intent(in) :: x
    real(REAL32) :: u

    u = abs(x)
    if( u < a )then
       ihampel = x**2 / 2
    else if( a <= u .and. u < b )then
       ihampel = a*(u - a/2)
    else if( b <= u .and. u < c )then
       ihampel = q*u*(c - u/2) + d
    else !if( u >= c )then
       ihampel = six
    endif

  end function ihampel_REAL32

end module rfun_hampel_REAL32
